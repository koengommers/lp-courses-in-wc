<?php
/*
Plugin Name: LearnPress courses in WooCommerce
Description: Sell LearnPress courses with WooCommerce
Author: Koen Gommers
Version: 0.1

*/

add_action( 'woocommerce_grant_product_download_access', 'give_course_access', 10, 1 );
function give_course_access( $data ) {
    $product = wc_get_product( $data['product_id'] );
    $course_id = $product->get_attribute( 'course_id' );
    $user_id = $data['user_id'];
    $user = learn_press_get_user($user_id, false);

    $order = learn_press_create_order( [] );
    $order->set_customer_note( '' );
    $order->set_status( 'lp-completed' );
    $order->set_total( $product->get_price() );
    $order->set_subtotal( $product->get_price() );
    $order->set_user_ip_address( learn_press_get_ip() );
    $order->set_user_agent( learn_press_get_user_agent() );
    $order->set_created_via( 'external' );
    $order->set_user_id( $user_id );
    $order->save();
    $order->add_item( $course_id );

    $user->enroll( $course_id, $order->id );
}
